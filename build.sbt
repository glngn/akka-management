import java.nio.file.Paths

// root
lazy val `akka-management-root` = project
  .in(file("."))
  .enablePlugins(ScalaUnidocPlugin)
  .enablePlugins(NoPublish)
  .aggregate(
    `akka-discovery`,
    `akka-discovery-aggregrate`,
    `akka-discovery-aws-api`,
    `akka-discovery-aws-api-async`,
    `akka-discovery-config`,
    `akka-discovery-consul`,
    `akka-discovery-dns`,
    `akka-discovery-kubernetes-api`,
    `akka-discovery-marathon-api`,
    `akka-management`,
    `cluster-http`,
    `cluster-bootstrap`,
    docs
  )
  .settings(
    parallelExecution in GlobalScope := false,
    inThisBuild(List(
      resolvers += Resolver.sonatypeRepo("comtypesafe-2189")

    ))
  )

// interfaces and extension for Discovery
lazy val `akka-discovery` = project
  .in(file("discovery"))
  .settings(
    name := "akka-discovery",
    organization := "com.lightbend.akka.discovery",
    Dependencies.Discovery
  )

// DNS implementation of discovery, default and works well for Kubernetes among other things
lazy val `akka-discovery-dns` = project
  .in(file("discovery-dns"))
  .settings(
    name := "akka-discovery-dns",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryDns
  )
  .dependsOn(`akka-discovery`)

lazy val `akka-discovery-config` = project
  .in(file("discovery-config"))
  .settings(
    name := "akka-discovery-config",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryConfig
  )
  .dependsOn(`akka-discovery`)

lazy val `akka-discovery-aggregrate` = project
  .in(file("discovery-aggregate"))
  .settings(
    name := "akka-discovery-aggregate",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryAggregate
  )
  .dependsOn(`akka-discovery`)
  .dependsOn(`akka-discovery-config` % "test")

lazy val `akka-discovery-kubernetes-api` = project
  .in(file("discovery-kubernetes-api"))
  .settings(
    name := "akka-discovery-kubernetes-api",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryKubernetesApi
  )
  .dependsOn(`akka-discovery`)

// Marathon API implementation of discovery, allows port discovery and formation to work even when readiness/health checks are failing
lazy val `akka-discovery-marathon-api` = project
  .in(file("discovery-marathon-api"))
  .settings(
    name := "akka-discovery-marathon-api",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryMarathonApi
  )
  .dependsOn(`akka-discovery`)

// AWS implementation of discovery
lazy val `akka-discovery-aws-api` = project
  .in(file("discovery-aws-api"))
  .settings(
    name := "akka-discovery-aws-api",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryAwsApi
  )
  .dependsOn(`akka-discovery`)

// Non-blocking AWS implementation of discovery
lazy val `akka-discovery-aws-api-async` = project
  .in(file("discovery-aws-api-async"))
  .settings(
    name := "akka-discovery-aws-api-async",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryAwsApiAsync
  )
  .dependsOn(`akka-discovery`)

lazy val `akka-discovery-consul` = project
  .in(file("discovery-consul"))
  .settings(
    name := "akka-discovery-consul",
    organization := "com.lightbend.akka.discovery",
    Dependencies.DiscoveryConsul
  )
  .dependsOn(`akka-discovery`)

// gathers all enabled routes and serves them (HTTP or otherwise)
lazy val `akka-management` = project
  .in(file("management"))
  .settings(
    name := "akka-management",
    Dependencies.ManagementHttp
  )

// cluster management http routes, expose information and operations about the cluster
lazy val `cluster-http` = project
  .in(file("cluster-http"))
  .settings(
    name := "akka-management-cluster-http",
    Dependencies.ClusterHttp
  )
  .dependsOn(`akka-management`)

// cluster bootstraping
lazy val `cluster-bootstrap` = project
  .in(file("cluster-bootstrap"))
  .settings(
    name := "akka-management-cluster-bootstrap",
    Dependencies.ClusterBootstrap
  )
  .dependsOn(`akka-management`, `akka-discovery`)

lazy val docs = project
  .in(file("docs"))
  .enablePlugins(ParadoxPlugin, NoPublish)
  .settings(
    name := "Akka Management",
    paradoxGroups := Map("Language" -> Seq("Scala", "Java")),
    paradoxTheme := Some(builtinParadoxTheme("generic")),
    paradoxProperties ++= Map(
      "version" -> version.value,
      "scala.binary_version" -> scalaBinaryVersion.value,
      "extref.akka-docs.base_url" -> s"http://doc.akka.io/docs/akka/${Dependencies.AkkaVersion}/%s",
      "extref.akka-http-docs.base_url" -> s"http://doc.akka.io/docs/akka-http/${Dependencies.AkkaHttpVersion}/%s.html",
      "extref.java-api.base_url" -> "https://docs.oracle.com/javase/8/docs/api/index.html?%s.html",
      "scaladoc.akka.base_url" -> s"http://doc.akka.io/api/akka/${Dependencies.AkkaVersion}",
      "scaladoc.akka.management.http.base_url" -> {
        if (isSnapshot.value) Paths.get((target in paradox in Compile).value.getPath).relativize(Paths.get(".")).toString
        else s"http://developer.lightbend.com/docs/api/akka-management/${version.value}"
      },
      "scaladoc.version" -> "2.12.0"
    )
  )
