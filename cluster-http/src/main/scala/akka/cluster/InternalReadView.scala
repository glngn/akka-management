package akka.cluster

import akka.annotation.InternalApi

@InternalApi
object InternalReadView {
  def apply(cluster: Cluster): ClusterReadView = cluster.readView
}
