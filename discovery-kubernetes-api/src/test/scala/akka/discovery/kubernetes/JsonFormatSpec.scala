/*
 * Copyright (C) 2017 Lightbend Inc. <http://www.lightbend.com>
 */
package akka.discovery.kubernetes

import org.scalatest.{Matchers, WordSpec}
import spray.json._
import scala.io.Source

import PodList._

class JsonFormatSpec extends WordSpec with Matchers {
  "JsonFormat" should {
    val data = resourceAsString("pods.json")

    "work" in {
      JsonFormat
        .podListFormat
        .read(data.parseJson) shouldBe PodList(
          List(
            Pod(
              Some(
                PodSpec(
                  List(
                    Container(
                      "akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000), ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
                Some(PodStatus(Some("172.17.0.4"), "2017-12-06T15:30:22Z", "Running")), Some(Metadata(deletionTimestamp = None, name = "akka-cluster-tooling-example-v0-1-0-7f854bcc78-dvm9q"))),

            Pod(
              Some(
                PodSpec(
                  List(
                    Container(
                      "akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000), ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
                Some(PodStatus(Some("172.17.0.6"), "2017-12-06T15:30:22Z", "Running")), Some(Metadata(deletionTimestamp = None, name = "akka-cluster-tooling-example-v0-1-0-7f854bcc78-m8dqb"))),

            Pod(
              Some(
                PodSpec(
                  List(
                    Container(
                      "akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000), ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
                  Some(PodStatus(Some("172.17.0.7"), "2017-12-06T15:30:22Z", "Running")), Some(Metadata(deletionTimestamp = Some("2017-12-06T16:30:22Z"), name = "akka-cluster-tooling-example-v0-1-0-7f854bcc78-xncvj")))))
    }
  }


  private def resourceAsString(name: String): String =
    Source
      .fromInputStream(getClass.getClassLoader.getResourceAsStream(name))
      .mkString
}
