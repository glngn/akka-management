/*
 * Copyright (C) 2017 Lightbend Inc. <http://www.lightbend.com>
 */
package akka.discovery.kubernetes

import java.net.InetAddress
import java.time.Instant
import akka.discovery.SimpleServiceDiscovery.ResolvedTarget
import org.scalatest.{ Matchers, WordSpec }

import PodList._

class KubernetesApiSimpleServiceDiscoverySpec extends WordSpec with Matchers {
  val maxPodStartTime = Instant.parse("2018-10-02T18:51:57Z")

  "targets" should {
    "calculate the correct list of resolved targets" in {
      val podList =
        PodList(List(Pod(Some(PodSpec(List(Container("akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000),
                          ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
              Some(PodStatus(Some("172.17.0.4"), "2018-10-02T18:41:57Z", "Running")), Some(Metadata(deletionTimestamp = None, name = "pod-0"))),
            Pod(Some(PodSpec(List(Container("akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000),
                          ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
                Some(PodStatus(None, "2018-10-02T19:21:57Z", "Pending")), Some(Metadata(deletionTimestamp = None, name = "pod-1"))),
                     Pod(Some(PodSpec(List(Container("akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000),
                          ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
              Some(PodStatus(Some("172.17.0.5"), "2018-10-02T18:52:57Z", "Running")), Some(Metadata(deletionTimestamp = None, name = "pod-2")))))

      KubernetesApiSimpleServiceDiscovery.targets(podList,
                                                  "akka-mgmt-http",
                                                  "default",
                                                  "cluster.local",
                                                  maxPodStartTime) shouldBe List(
          ResolvedTarget(
            host = "172-17-0-4.default.pod.cluster.local",
            port = Some(10001),
            address = Some(InetAddress.getByName("172.17.0.4"))
          ))
    }

    "ignore deleted pods" in {
      val podList =
        PodList(List(Pod(Some(PodSpec(List(Container("akka-cluster-tooling-example",
                      Some(List(ContainerPort(Some("akka-remote"), 10000),
                          ContainerPort(Some("akka-mgmt-http"), 10001), ContainerPort(Some("http"), 10002))))))),
              Some(PodStatus(Some("172.17.0.4"), "2018-10-02T18:21:57Z", "Succeeded")), Some(Metadata(deletionTimestamp = Some("2017-12-06T16:30:22Z"), name = "pod-ax82")))))

      KubernetesApiSimpleServiceDiscovery.targets(podList,
                                                  "akka-mgmt-http",
                                                  "default",
                                                  "cluster.local",
                                                  maxPodStartTime) shouldBe List.empty
    }
  }
}
